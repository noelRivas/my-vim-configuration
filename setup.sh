# Remove the bundles and start fresh
rm -rf bundle
mkdir bundle


# Alias this repo as ~/.vim
echo ""
echo "************************************"
echo "Aliasing this repo as ~/.vim"
echo "************************************"
echo ""
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
ln -s "$DIR" ~/.vim

# Copy .vimrc
echo ""
echo "************************************"
echo "Installing CtrlP"
echo "************************************"
echo ""
cp vimrc ~/.vimrc

# Install CtrlP
echo ""
echo "************************************"
echo "Installing CtrlP"
echo "************************************"
echo ""
git clone https://github.com/kien/ctrlp.vim.git bundle/ctrlp.vim

# Install Emmet
echo ""
echo "************************************"
echo "Installing Emmet"
echo "************************************"
echo ""
git clone https://github.com/mattn/emmet-vim.git bundle/emmet-vim

# Auto-pairs
echo ""
echo "************************************"
echo "Installing auto-pairs"
echo "************************************"
echo ""
git clone git://github.com/jiangmiao/auto-pairs.git bundle/auto-pairs

# Lightline
echo ""
echo "************************************"
echo "Installing Lightline"
echo "************************************"
echo ""
git clone https://github.com/itchyny/lightline.vim bundle/lightline.vim

# Easymotion
echo ""
echo "************************************"
echo "Installing easymotion"
echo "************************************"
echo ""
git clone https://github.com/Lokaltog/vim-easymotion bundle/vim-easymotion

# Surround
echo ""
echo "************************************"
echo "Installing Surround"
echo "************************************"
echo ""
git clone git://github.com/tpope/vim-surround.git bundle/vim-surround


