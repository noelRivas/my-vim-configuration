" Enable solarized color scheme
syntax enable
set background=light
colorscheme solarized

nmap <leader>mmm :set lines=999 columns=999<cr>
