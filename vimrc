" Pathogen
execute pathogen#infect()

" With a map leader it's possible to do extra key combinations
" like <leader>w saves the current file
let mapleader = ","
let g:mapleader = ","

" Fast saving and quitting
nmap <leader>w :w!<cr>
nmap <leader>q :q<cr>

" Fast access to search
nmap <leader>f :/

" Fast move to next buffer
nmap <leader>n :bnext<cr>

" Fast close current buffer
nmap <leader>. :BD<cr>

" Move between splits
nmap <leader><Space> <c-w><c-w>
nmap <leader>j <c-w><c-j>
nmap <leader>k <c-w><c-k>
nmap <leader>h <c-w><c-h>
nmap <leader>l <c-w><c-l>

" Expand splits or distribute them equally
nmap <leader>+ 100<c-w>+
nmap <leader>- <c-w>=

" Faster Emmet
" nmap <leader>e <c-y>,

" Fast syntax selection
" PHP
nmap <leader>sp :set syntax=php<cr>

" Fast NERDTree opening
nmap <leader>t :NERDTreeToggle<cr>

" Fast vdebug breakpoint toggle
nmap <leader>* :Breakpoint<cr>

" Quick folding
nmap <leader>ff vi{zf

" CWD vim to NerdTree initial dir
let g:NERDTreeChDirMode=1

" INDENTATION
set expandtab
set shiftwidth=2
set softtabstop=2

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" Enable syntax highlighting
syntax enable

" Set to auto read when a file is changed from the outside
set autoread

" Ignore case when searching
set ignorecase

"Always show current position
set ruler

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Set relative line numbers
set rnu

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Enable filetype plugins?
filetype plugin on

" Testing this GPG thing...
let g:GPGPreferArmor=1
let g:GPGDefaultRecipients=["josue@matrushka.com.mx,noel@matrushka.com.mx"]

" Taken from https://nathanleclaire.com/blog/2014/01/20/speed-up-your-workflow-by-running-phpunit-tests-inside-of-vim/
function! RunPHPUnitTest()
    cd %:p:h
    let result = system("phpunit " . bufname("%"))
    split __PHPUnit_Result__
    normal! ggdG
    setlocal buftype=nofile
    call append(0, split(result, '\v\n'))
    cd -
endfunction

nnoremap <leader>u :call RunPHPUnitTest()<cr>

" Instant markdown configuration
let g:instant_markdown_autostart = 0
let g:instant_markdown_slow = 1

function! SetXDEBUGPathMapping()
  " Ask for the path to map /app to, and provide CWD as the default
  let g:_mapsource = input('Remote project root: ', '/var/www/html')
  let g:_maptarget = input('Map remote ' . g:_mapsource . ' to: ', getcwd())
  let g:vdebug_options.path_maps = {g:_mapsource: g:_maptarget} 
  echo "XDEBUG will now look for paths in " . g:_mapsource . " at:"
  echo g:_maptarget
  echo g:vdebug_options.path_maps
endfunction

nnoremap <leader>sx :call SetXDEBUGPathMapping()<cr>

" Ignore vendor and .git directories in CtrlP
let g:ctrlp_custom_ignore = '\v[\/](node_modules|vendor)|(\.(git))$'
