# README #

This is a simple script to easily configure Vim the way I like it in any machine I have a user on.

**WARNING: These steps will delete your current vim configuration.**

To configure Vim:
```sh
cd ~
rm -rf .vim
rm .vimrc
git clone https://nelovishk@bitbucket.org/nelovishk/my-vim-configuration.git
my-vim-configuration/setup.sh
```


Vim will then have the following plugins:

* [emmet-vim](http://mattn.github.io/emmet-vim/)
* [auto-pairs](https://github.com/jiangmiao/auto-pairs)
* [ctrlp](https://github.com/kien/ctrlp.vim)
* [lightline](https://github.com/itchyny/lightline.vim)
* [vim-easymotion](https://github.com/Lokaltog/vim-easymotion)
* [vim-surround](https://github.com/tpope/vim-surround)